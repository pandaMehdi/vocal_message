import { 
  MESSAGE_UPDATE
} from './types';

export const messageUpdate = ({ prop, value }) => {
  return {
    type: MESSAGE_UPDATE,
    payload: { prop, value }
  };
};
import { 
  RECORD_UPDATE,
  RECORDING_IN_PROGRESS
} from './types';

export const recordUpdate = (path) => {
  return {
    type: RECORD_UPDATE,
    payload: path
  };
};

export const recording = (value) => {
  return {
    type: RECORDING_IN_PROGRESS,
    payload: value
  };
};
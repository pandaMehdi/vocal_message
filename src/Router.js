import React from 'react';
import { StyleSheet } from 'react-native';
import { Scene, Router, Actions, Tabs } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import MessageForm from './components/MessageForm';
import TabIcon from './components/common/TabIcon';
import EmployeeList from './components/EmployeeList';
import EmployeeCreate from './components/EmployeeCreate';
import EmployeeEdit from './components/EmployeeEdit';

const RouterComponent = () => {
  return <Router>
      <Scene key="root" hideNavBar>
        <Scene key="auth">
          <Scene key="login" component={LoginForm} title="Login" initial />
        </Scene>
        <Scene key="main">
          <Scene key="root" tabs={true} initial>
            <Scene hideNavBar={true}  key="MessageForm" component={MessageForm} imgIcon="phone" icon={TabIcon} />
            <Scene key="EmployeeList" title="Employee List" rightTitle="Add" onRight={() => Actions.employeeCreate()} component={EmployeeList} icon={TabIcon} />
          </Scene>
          <Scene key="employeeCreate" component={EmployeeCreate} title="Create employee" />
          <Scene key="employeeEdit" component={EmployeeEdit} title="Edit employee" />
        </Scene>
      </Scene>
    </Router>;
};

export default RouterComponent;
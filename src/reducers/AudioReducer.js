import {
  RECORD_UPDATE,
  RECORDING_IN_PROGRESS
} from '../actions/types';
import react from 'react';

const INITIAL_STATE = {
  path: '',
  recording: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
      case RECORD_UPDATE:
          return {
              ...state,
              path: action.payload
          }
      case RECORDING_IN_PROGRESS:
          return {
            ...state,
            recording: action.payload
          }
      default:
          return state;
  }
};
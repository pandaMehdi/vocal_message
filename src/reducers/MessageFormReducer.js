import {
  MESSAGE_UPDATE,
} from '../actions/types';

const INITIAL_STATE = {
  name: '',
  phone: '',
  shift: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
      case MESSAGE_UPDATE:
          //action.payload === { props: 'name', value: 'jane' } 
          return {
              ...state,
              [action.payload.prop]: action.payload.value
          }
      default:
          return state;
  }
};
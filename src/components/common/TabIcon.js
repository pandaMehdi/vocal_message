import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View
} from 'react-native';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/MaterialIcons'

const propTypes = {
  selected: PropTypes.bool,
  imgIcon: PropTypes.string,
};

const TabIcon = (props) => {

  return (
    <View style={{alignItems: 'center', justifyContent: 'center'}}>
      <Icon color={props.focused ? '#A7EFE9': '#7FDFD4'} name={props.imgIcon} size={30}/>
    </View>
  )
};

TabIcon.propTypes = propTypes;

export default TabIcon;

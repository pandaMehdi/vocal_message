import React, { Component } from 'react';
import { View, Text, Picker } from 'react-native';
import { AppRegistry, AudioRecorder} from 'react-native-audio';
import { connect } from 'react-redux';
import { messageUpdate } from '../actions';
import { CardSection, Card, Input, Button } from './common';
import AudioController from './AudioController';

class MessageForm extends Component {
  render() {  
    const isCompleted = this.props.name !== '' && this.props.phone !== '' && this.props.path !== '' && !this.props.recording;
    const styles = {
      view: {
        flexDirection: 'row',
        height: 80,
      },
      wrapperText: {
        flex: 0, 
        flexGrow: 1, 
        justifyContent: 'center',
        alignItems: 'center',
      }
    };
    return (
      <View>
        <View style={styles.view}>
                <View style={{...styles.wrapperText, backgroundColor: "#0d3b5a"}}><Text style={{color: '#ffffff'}}>Destinataire</Text></View>
                <View style={{...styles.wrapperText, backgroundColor: "#7ebfbe"}}><Text style={{color: '#ffffff'}}>Envoyer</Text></View>
        </View>
        <Card>
          <CardSection>
            <Input
              label="Destinataire"
              placeholder="Jane"
              value={this.props.name}
              onChangeText={value => this.props.messageUpdate({
                prop: 'name',
                value
              })}
            />
          </CardSection>
          <CardSection>
            <Input
              label="Numéro"
              placeholder="555-555-5555"
              keyboardType="numeric"
              value={this.props.phone}
              onChangeText={value => this.props.messageUpdate({
                prop: 'phone',
                value
              })}
            />
          </CardSection>
          <CardSection>
            <AudioController />
          </CardSection>
          {isCompleted && 
            <CardSection>
              <Button>Envoyer votre message</Button>
            </CardSection>
          }
        </Card>
      </View>
    );
  }
};

const mapStateToProps = (state) => {
  const { name, phone, shift } = state.message;
  const { path, recording } = state.audio;
  return { name, phone, shift, path, recording };
};
 
export default connect(mapStateToProps, { messageUpdate })(MessageForm);